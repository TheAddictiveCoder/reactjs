import React from 'react';
import { render, cleanup } from '@testing-library/react';
import App from '../App';

describe('App component should', () => {
  afterEach(cleanup);

  it('renders layout corretly', () => {
    const { asFragment } = render(<App />);
    expect(asFragment()).toMatchSnapshot();
  });

  it('renders learn react link', () => {
    const { getByText } = render(<App />);

    expect(getByText('Learn React')).toBeInTheDocument();
  });
})