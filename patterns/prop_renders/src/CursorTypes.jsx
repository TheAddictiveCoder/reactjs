import React from 'react';

class Cursor extends React.Component {
  render() {
    const type = this.props.cursorType;
    return (
      <img src={process.env.PUBLIC_URL + '/images/cursor.png'} alt="hello" style={{ position: 'absolute', left: type.x, top: type.y }} />
    );
  }
}

class Finger extends React.Component {
  render() {
    const type = this.props.cursorType;
    return (
      <img src={process.env.PUBLIC_URL + '/images/finger.png'} alt="" style={{ position: 'absolute', left: type.x, top: type.y }} />
    );
  }
}

export {Cursor, Finger};
