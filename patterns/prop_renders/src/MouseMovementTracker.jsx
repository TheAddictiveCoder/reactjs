import React from 'react';
import TrackerFunction from './TrackerFunction';
// eslint-disable-next-line
import { Cursor, Finger } from './CursorTypes';

class MouseMovementTracker extends React.Component {
  render() {
    return (
      <div>
        <TrackerFunction render={typeToRender => 
          (<Cursor cursorType={typeToRender} />)
        } 
        />
        {/* <TrackerFunction render={renderType => 
          (<Finger renderType={renderType} />)
        }/> */}
      </div>
    );
  }
}

export default MouseMovementTracker;
