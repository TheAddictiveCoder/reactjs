import React from 'react';

class TrackerFunction extends React.Component {
    constructor(props) {
      super(props);
      this.handleMouseMove = this.handleMouseMove.bind(this);
      this.state = { x: 10, y: 50 };
    }
  
    handleMouseMove(event) {
      this.setState({
        x: event.clientX,
        y: event.clientY
      });
    }
  
    render() {
      return (
        <div style={{ height: '100vh' }} onMouseMove={this.handleMouseMove}>
          <p>The current position is (X: {this.state.x}, Y: {this.state.y})</p>
          {this.props.render(this.state)}
        </div>
      );
    }
  }

  export default TrackerFunction;
